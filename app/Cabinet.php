<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cabinet extends Model
{
    public function getTotalPriceForItem($orders)
    {
        $totalPrice = 0;
        foreach($orders as $order)
        {
            $totalPrice = 0;
            foreach($order->orderItems as $item)
            {
                $totalPrice += $item->price * $item->quantity;
            }
            $order->totalPrice = number_format($totalPrice, 2, ',', ' ');
        }
        return $orders;
    }

    public function formatPrice($order)
    {
        foreach($order as $item)
            {
                $item->price = number_format($item->price, 2, ',', ' ');
            }

        return $order;
    }

    public function convertStatus($orders)
    {
        foreach($orders as $order)
        {
            switch($order->status)
            {
                case "new": $order->status = "новый";
                    break;
                case "confirmed": $order->status = "заказ подтвержден";
                    break;
                case "prepare to sent": $order->status = "формерование заказа";
                    break;
                case "rady to sent": $order->status = "готов к отправке";
                    break;
                case "sended":
                case "confirmed reciving":
                case "paid": $order->status = "отправлен";
                    break;
                case "successfully closed": $order->status = "успешно закрыт";
                    break;
            }
        }

        return $orders;
    }

    public function decodeJson($items)
    {  
        if(count($items)>1)
        {
            foreach ($items as $item)
            {
                if($item->product->images != null)
                {
                   $item->images = json_decode($item->product->images);
                } else
                {
                    $item->images = [0 =>'no-image.png'];
                }
            }
        } else
        {
            if($items[0]->product->images != null)
            {
                   $items[0]->images = json_decode($items[0]->product->images);
            } else
            {
                    $items[0]->images = [0 =>'no-image.png'];
            }
        }
        return $items;
    }

    public function getTotalPriceOfOrder($order)
    {
        $totalPriceForItem = 0;
        foreach($order as $item)
            {
                $totalPriceForItem += $item->price * $item->quantity;
            }

        $totalPriceForItem = number_format($totalPriceForItem, 2, ',', ' ');

        return $totalPriceForItem;
    }
}
