<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public static function addItem($id, $quantity)
    {
        if (\Session::has($id))
        {
            \Session::put($id, (\Session::get($id)+$quantity));
        }else
        {
            \Session::put($id, $quantity);
        }
    }

    public function getItemsListForCrat()
    {

         if(\Session::get('items'))
        {
            $param = \Session::get('items');
            $items = [];
            $i = 0;
            foreach ($param as $item)
            {
                $items[$i]           = Product::where('id', key($item))->first();
                $items[$i]           = self::getPriceWithDiscount($items[$i]);
                $items[$i]->quantity = current($item);
                $items[$i]           = self::decodeJson($items[$i]);
                $i++;
            }
        }else
        {
            $items = null;
        }
        return $items;
    }

    public function getTotalPrice($items)
    {
        $totalPrice = 0;
        foreach($items as $item)
        {
            $totalPrice += $item->price * $item->quantity;
        }
        return $totalPrice;
    }

    public function decodeJson($products)
    {
        if(count($products)>1)
        {
            foreach ($products as $product)
            {
                if($product->images != null)
                {
                   $product->images = json_decode($product->images);
                } else
                {
                    $product->images = [0 =>'no-image.png'];
                }
            }
        } else
        {
            if($products->images != null)
                {
                   $products->images = json_decode($products->images);
                } else
                {
                    $products->images = [0 =>'no-image.png'];
                }
        }
        return $products;
    }

    public function getPriceWithDiscount($products)
    {
        if(count($products) > 1)
        {
            foreach($products as $product)
            {
                $product->discountPrice = 0;
                if($product->discount > 0)
                {
                    $product->discountPrice = round($product->price - $product->price*$product->discount/100, 2);
                }
            }
        }else
        {
            $products->price = round($products->price - $products->price*$products->discount/100, 2);
        }

        return $products;
    }

    //add Item to cart
    public function addItemToCart($id, $quantity)
    {
        $arr = \Session::get('items');
        $i = 0;
        if($arr != null)
        {
          foreach($arr as $item)
        {
            if(isset($item[$id]))
            {
                $arr[$i] = [$id => $quantity];
                \Session::put('items', $arr);
                return self::getTotalItemsInCart();
            }
            $i++;
        }  
        }
        \Session::push('items', [$_POST['id'] => $_POST['quantity']]);
        return self::getTotalItemsInCart();
    }

    /*
     * remove Item in cart
     */
    public function removeItemInCart($id)
    {
        $newArr = array();
        $arr = \Session::get('items');
        $i = 0;
        foreach($arr as $item)
        {
            if(isset($item[$id]))
            {
                unset($arr[$i]);
            }
            $i++;
        }
        foreach($arr as $value)
        {
            array_push($newArr, $value);
        }
        \Session::put('items', $newArr);
    }
    
    public function updateItemQuantityInCart($quantity, $id)
    {
        $arr = \Session::get('items');
        $i = 0;
        foreach($arr as $item)
        {
            if(isset($item[$id]))
            {
                $arr[$i] = [$id => $quantity];
            }
            $i++;
        }
        \Session::put('items', $arr);
    }

    /**
     * Подсчет количество товаров в корзине (в сессии)
     * @return int
     */
    public function getTotalItemsInCart()
    {
        $totalItems = 0;
        if(\Session::get('items'))
        {
            $arr = \Session::get('items');
            foreach($arr as $item)
            {
                foreach($item as $key => $value)
                {
                    $totalItems += $value;
                }
            }
            return $totalItems;
        }
        return 0;
    }

    /*
     * clean Cart
     */
    public function cleanCart()
    {
        \Session::forget('items');
    }
}
