<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Category extends Model
{
    use Sluggable;

    const IS_NOT_DISPLAYED = 0;

    protected $fillable = ['discount', 'is_displayed'];

    public function subcategories()
    {
        return  $this->hasMany(Subcategory::class);
    }

    public function products()
    {
        return  $this->hasMany(Product::class);
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public static function add($fields)
    {
        $category = new static;

        $category->isDisplayed($fields);

        $category->name = $fields['name'];
        $category->fill($fields);
        $category->save();
    }

    public function edit($fields)
    {
        if($fields['name'] != null)
        {
            $this->slug = null;
            $this->name = $fields['name'];
        }

        $this->isDisplayed($fields);

        $this->fill($fields);
        $this->save();
    }
    
    public function isDisplayed($fields)
    {
        if(!array_key_exists('is_displayed', $fields))
        {
            $this->is_displayed = Category::IS_NOT_DISPLAYED;
        }
    }
}
