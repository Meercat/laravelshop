<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\OrderItem;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        $obj = new Order;
        foreach($orders as $order)
        {
          $order->orderPrice = $obj->getOrderPrice($order);
        }
        return view('admin.orders.index', ['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::find($id);
        $products = Product::all();
        return view('admin.orders.edit', ['order' => $order, 'products' => $products]);
    }

    /**
     * Update order.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request ,[
            'surname' => 'required',
            'name' => 'required',
            'second_name' => 'required',
            'address' => 'required',
        ]);
        $order = Order::find($id);
        $order = $order->editOrder($request->all(), $id);

        return redirect()->route('orders.index');
    }

    /**
     * Remove order.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        
    }
    /**
     * Remove Item from order.
     */
    public function destroyItem()
    {
        $id = $_POST['idItem'];
        $item = OrderItem::find($id);
        $item->delete();
    }
     /**
     * Add new Item in order.
     */
    public function addNewItemToOrder(Request $request, $id)
    {
        var_dump($id);
        dd($request);
        $product = Product::find($id);
        $newItem = new OrderItem;
        $newItem->addItem($request->order_id,$product, $request->quantity);
        return redirect()->back();
    }
}
