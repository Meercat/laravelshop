<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    public function index()
    {
//        $productsWithJsonImages = Product::all();
        $productsWithJsonImages = Product::paginate(100);
        $obj = new Product;
        $products = $obj->decodeJson($productsWithJsonImages);
        return view('admin.products.index', ['products' => $products]);
    }

    public function create()
    {
        $categories = Category::all();
        $subcategories = Subcategory::all();
        return view('admin.products.create', [
            'categories' => $categories,
            'subcategories' => $subcategories
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'unite_of_measure' => 'required',
            'description' => 'required'
        ]);
        $product = Product::add($request->all());
        $images = Product::find($product->id);
        $images->uploadImages($request->images, $product->id);

        return redirect()->route('products.index');
    }
    
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();
        $subcategories = Subcategory::all();
        return view('admin.products.edit', ['product' => $product,
            'categories' => $categories,
            'subcategories' => $subcategories]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric',
            'discount' => 'required|numeric',
            'vendor' => 'required',
        ]);
        
        $productUpDate = Product::find($id);
        
        if($request->images == null)
        {
            $request->images = $productUpDate->images;
        }
        $productUpDate->editProduct($request->all());
        $productUpDate->updateImages($request->images, $productUpDate);
        return redirect()->route('products.index');
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->removeProduct($product);
        return redirect()->route('products.index');
    }


    public function updatePriceFromElektreka()
    {
        $update = new Product;
        $update->updatePriceFromElektreka();

        return redirect()->route('products.index');
    }
}
