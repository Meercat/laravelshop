<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubcategoriesController extends Controller
{
    public function index()
    {
        $subcategories = Subcategory::all();
        return view('admin.subcategories.index', ['subcategories' => $subcategories]);
    }

    public function create()
    {
        $categories = Category::all();
        return view('admin.subcategories.create', ['categories' => $categories]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
           'name' => 'required|unique:subcategories',
       ]);

        Subcategory::add($request->all());
        return redirect()->route('subcategories.index');
    }

    public function edit($id)
    {
        $subcategory = Subcategory::find($id);
        $categories = Category::all();
        return view('admin.subcategories.edit', ['subcategory' => $subcategory, 'categories' => $categories]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'unique:subcategories',
        ]);

        $subcategory = Subcategory::find($id);
        $subcategory->edit($request->all());
        return redirect()->route('subcategories.index');
    }

    public function destroy($id)
    {
        Subcategory::find($id)->delete();
        return redirect()->route('subcategories.index');

    }
}
