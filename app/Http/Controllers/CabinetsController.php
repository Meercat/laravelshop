<?php

namespace App\Http\Controllers;

use App\Order;
use App\Cabinet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CabinetsController extends Controller
{
    public function index()
    {
        $cabinetObj = new Cabinet;
        $orders = Order::where([
            ['users_id', \Auth::user()->id],
            ['status', '!=', 'successfully closed']
            ])
           ->paginate(4);
        $orders = $cabinetObj->getTotalPriceForItem($orders);
        $orders = $cabinetObj->convertStatus($orders);

        return view('pages.cabinet', ['orders' => $orders]);
    }

    //get all orders of user in cabinet
    public function getAllOrders()
    {
        $cabinetObj = new Cabinet;
        $orders = Order::where('users_id', \Auth::user()->id)->paginate(4);
        $orders = $cabinetObj->getTotalPriceForItem($orders);
        $orders = $cabinetObj->convertStatus($orders);

        return view('pages.cabinet', ['orders' => $orders]);
    }

    //get details of order
    public function getOrderDetails($orderId)
    {

        $cabinetObj = new Cabinet;
        $order = Order::where('id', $orderId)->firstOrFail();
        
        if(Auth::id() != $order->users_id)
        {
            return redirect()->back();
        }

        $items = $cabinetObj->decodeJson($order->orderItems);
        $totalPriceOfOrder = $cabinetObj->getTotalPriceOfOrder($items);
        $items = $cabinetObj->formatPrice($order->orderItems);

        return view('pages.order-details', ['order' => $order, 'items' => $items, 'totalPriceOfOrder' => $totalPriceOfOrder, 'orderId' => $orderId]);
    }
}
