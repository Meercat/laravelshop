<?php

namespace App\Http\Controllers;


use App\Cart;
use App\Product;
use Illuminate\Http\Request;

class CartsController extends Controller
{
    // show cart
    public function index()
    {
        $cartObj = new Cart;
        $items = $cartObj->getItemsListForCrat();

        if($items != null)
        {
            $totalPrice = $cartObj->getTotalPrice($items);
        }
        else
        {
            $totalPrice = 0;
        }

        return view('pages.cart', ['items' => $items, 'totalPrice' => $totalPrice]);
    }

    //add Items to cart
    public function addItemAjax()
    {
       $obj = new Cart;
       echo $obj->addItemToCart($_POST['id'], $_POST['quantity']);
    }

    //remove Item in cart
    public function removeItemInCart($id)
    {
        $obj = new Cart;
        $obj->removeItemInCart($id);

        return redirect()->route('carts.index');
    }

    //update Item quantity in cart
    public function updateItemQuantityInCart(Request $request, $id)
    {
        $obj = new Cart;
        $obj->updateItemQuantityInCart($request->quantity, $id);

        return redirect()->route('carts.index');
    }

    //remove all items from cart
    public function removeAllItems()
    {
        $cart = new Cart;
        $cart->cleanCart();

        return redirect()->route('index');
    }
}
