<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderItem;
use App\Cart;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\NewOrderMailForAdmin;

class OrdersController extends Controller
{
    public function addNewOrder(Request $request)
    {
         $this->validate($request, [
            'surname' => 'required',
            'name' => 'required',
            'second_name' => 'required',
            'phone' => 'required',
            'delivery' => 'required',
            'address' => 'required'
        ]);

        $cartObj = new Cart;
        //add new order
        $orderId = Order::addOrder($request->all());

        //add orderItems
        OrderItem::addItemsInOrder($orderId);

        //send email
        $comment = 'Это сообщение отправлено из формы обратной связи';
        $toEmail = "alekseev.surik@gmail.com";
        Mail::to($toEmail)->send(new NewOrderMailForAdmin($orderId));

        //clean cart
        $cartObj->cleanCart();
        return redirect()->route('carts.index');
    }
}
