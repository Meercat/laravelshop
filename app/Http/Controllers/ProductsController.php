<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Subcategory;
use App\User;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
        $obj = new Product;
        //get recommended products list
        $productsForMeanPage = Product::where('recommended', 1)->get();

        if(count($productsForMeanPage) == 0)
        {
            $productsForMeanPage = Product::all()->random(18);
        }
        //formate price
        $productsForMeanPage = $obj->formatPrice($productsForMeanPage);
        $productsForMeanPage =$obj->getPriceWithDiscount($productsForMeanPage);
        //get recommended products list where $productsForMeanPage->images is array
        $productsForMeanPage = $obj->decodeJson($productsForMeanPage);

        //get unique category Ids fro recommended items
        $categoriesId = $productsForMeanPage->unique('categories_id')->pluck('categories_id');
        //get categoryes where Ids in $categoriesId
        $categories = Category::whereIn('id', $categoriesId->toArray())->get();

        //get rendon products list
        $rendomProducts = Product::all()->random(18);
        //formate price
        $rendomProducts = $obj->formatPrice($rendomProducts);
        $rendomProducts = $obj->getPriceWithDiscount($rendomProducts);
        //get rendon products list where $rendomProducts->images is array
        $rendomProducts = $obj->decodeJson($rendomProducts)->chunk(3);

        return view('pages.index', [
            'categoriesOfProductsForMeanPage' => $categories,
            'productsForMeanPage' => $productsForMeanPage,
            'rendomProducts' => $rendomProducts
            ]);
    }

    public function getProductsOfCategory($productsOnPage, $slug)
    {
        $obj = new Product;
        $category = Category::where('slug', $slug)->firstOrFail();
        $products = Product::where('categories_id', $category->id)->paginate($productsOnPage);
        //formate price
        $products = $obj->formatPrice($products);

        if(count($products) > 0)
        {
            $products->setPath($category->slug);
            $products = $obj->decodeJson($products);
            $products = $obj->getPriceWithDiscount($products);
            return view('pages.products-of-category', [
                'category' => $category,
                'products' => $products,
                'productsOnPage' =>$productsOnPage]);
        }

        return view('pages.category-without-products', ['category' => $category]);
        
    }

    public function getProductsOfSubcategory($productsOnPage, $slug)
    {
        $obj = new Product;
        $subcategory = Subcategory::where('slug', $slug)->firstOrFail();
        $products = Product::where('subcategories_id', $subcategory->id)->paginate($productsOnPage);
        //formate price
        $products = $obj->formatPrice($products);
        
        if(count($products) > 0)
        {
            $products = $obj->decodeJson($products);
            $products = $obj->getPriceWithDiscount($products);
            return view('pages.products-of-subcategory', [
                'subcategory' => $subcategory,
                'products' => $products,
                'productsOnPage' =>$productsOnPage]);
        }
        return view('pages.subcategory-without-products', ['subcategory' => $subcategory]);
    }

    public function getProductDetails($slug)
    {
        $obj = new Product;
        // get product details
        $product = Product::where('slug', $slug)->firstOrFail();
        //formate price
        $product = $obj->formatPrice($product);
        $product = $obj->decodeJson($product);
        $product = $obj->getPriceWithDiscount($product);

        //get similar products from same category
        $similarProducts = Product::all()->where('categories_id', $product->categories_id)->random(27);
        //formate price
        $similarProducts = $obj->formatPrice($similarProducts);
        $similarProducts = $obj->getPriceWithDiscount($similarProducts);
        $similarProducts = $obj->decodeJson($similarProducts)->chunk(3);
        
        return view('pages.product-details',[
            'product' => $product,
            'similarProducts' => $similarProducts
            ]);
    }

    public function search(Request $request)
    {
        $productsObj = new Product;
        $products = Product::where('name', 'LIKE', '%' . $request->search . '%')->get();

        if(count($products) ==0)
        {
            $products = null;
            return view('pages.search-products-result', ['products' => $products]);
        }
        //formate price
        $products = $productsObj->formatPrice($products);

        $products = $productsObj->decodeJson($products);
        $products = $productsObj->getPriceWithDiscount($products);
        return view('pages.search-products-result', ['products' => $products]);

        
    }
}
