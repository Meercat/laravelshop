<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $fillable = [
        'name',
        'second_name',
        'surname',
        'phone',
        'email',
        'delivery',
        'address',
        'admin_comments',
        'user_comments'
        ];

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class, 'orders_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function getOrderPrice($order)
    {
        $orderPositionPrice = 0;

        foreach($order->orderItems as $orderItem)
        {
            $orderPositionPrice += $orderItem->quantity * ($orderItem->price - ($orderItem->price * $orderItem->product_discount / 100));
        }
        return $orderPositionPrice;
    }

    public static function addOrder($fields)
    {
        $order = new static;
        $order->fill($fields);
        if(\Auth::check())
        {
            $order->users_id = \Auth::user()->id;
        }
        $order->save();

        return $order->id;
    }
    public function editOrder($fields)
    {
        $this->fill($fields);
        $this->save();
    }
}
