<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    public function order()
    {
        return $this->belongsTo(Order::class, 'orders_id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class, 'products_id');
    }

    /*
     * add item in present order from admin-panel
     */
    public function addItem($orderId, $product, $quantity)
    {
        $item = new static;
        $item->orders_id = $orderId;
        $item->products_id = $product->id;
        $item->price = $product->price;
        $item->product_discount = $product->discount;
        $item->quantity = $quantity;
        $item->save();
    }

    /*
     * add items in new order
     */
    public static function addItemsInOrder($orderId)
    {
        $cartDatas = \Session::get('items');
        $item = new static;
        foreach($cartDatas as $data)
        {
            
            $product = $item->getProduct($data);
            $quantity = current($data);
            $item->addItem($orderId, $product, $quantity);
        }
    }

    /*
     * get product data for current item
     */
    public function getProduct($item)
    {
        $product = Product::where('id', key($item))->first();
        $product->price = round($product->price - ($product->price * $product->discount / 100), 2);
        
        return $product;
    }
}
