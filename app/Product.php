<?php

namespace App;

ini_set('max_execution_time', 1800);

use \Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    use Sluggable;
    

    const IS_NOT_DISPLAYED = 0;
    const IS_NOT_AVAILABLE = 0;
    const IS_NOT_RECOMMENDED = 0;
    const IS_NOT_HOT_DEAL = 0;
    const IS_NOT_BESTSELLER = 0;

    protected $fillable = [
        'name',
        'categories_id',
        'subcategories_id',
        'price',
        'available',
        'recommended',
        'is_displayed',
        'hot_deals',
        'bestsellers',
        'unite_of_measure',
        'vendor',
        'description'
        ];

    public function category ()
    {
        return $this->belongsTo(Category::class, 'categories_id');
    }

    public function subcategory ()
    {
        return $this->belongsTo(Subcategory::class, 'subcategories_id');
    }

    public function orderItems ()
    {
        return $this->hasMany(OrderItem::class, 'products_id');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public static function add($fields)
    {
        $product = new static;
        $product->issetAvailable($fields);
        $product->issetRecommended($fields);
        $product->issetDisplayed($fields);
        $product->issetHotDeal($fields);
        $product->issetBestseller($fields);
        $product->fill($fields);
        $product->save();

        return $product;
    }
    
    public function editProduct($fields)
    {
        $this->issetAvailable($fields);
        $this->issetRecommended($fields);
        $this->issetDisplayed($fields);
        $this->issetHotDeal($fields);
        $this->issetBestseller($fields);
        $this->fill($fields);
        $this->save();
    }
    
    public function issetAvailable($fields)
    {
        if(!array_key_exists('available', $fields))
        {
            $this->available = self::IS_NOT_DISPLAYED;
        }
    }
    
    public function issetRecommended($fields)
    {
        if(!array_key_exists('recommended', $fields))
        {
            $this->recommended = self::IS_NOT_DISPLAYED;
        }
    }
    
    public function issetDisplayed($fields)
    {
        if(!array_key_exists('is_displayed', $fields))
        {
            $this->is_displayed = self::IS_NOT_DISPLAYED;
        }
    }

    public function issetHotdeal($fields)
    {
        if(!array_key_exists('hot_deals', $fields))
        {
            $this->hot_deals = self::IS_NOT_HOT_DEAL;
        }
    }

    public function issetBestseller($fields)
    {
        if(!array_key_exists('bestsellers', $fields))
        {
            $this->bestsellers = self::IS_NOT_BESTSELLER;
        }
    }

    public function removeProduct($product)
    {
        //спочавтку видалити картинки
        $this->removeImages($product);
        //потім видаляємо товар
        $this->delete();
    }

    public function removeImages($product)
    {
        $images = json_decode($product->images);
        foreach ($images as $image) {
            if ($image != 'no-image.png') {
                \File::delete(public_path('uploads/'.$image));
            }
        }
    }

    public function uploadImages($arrImages, $id)
    {
        if($arrImages != null)
        {
            $arrImageNames = [];
            $i             = 0;
            foreach ($arrImages as $image) {
                $fileName = $id.'-'.$i.'.'.$image->extension();
                $image->move('uploads', $fileName);
                $arrImageNames[$i] = $fileName;
                $i++;
            }
            $jsonImages = json_encode($arrImageNames);
            $this->images = $jsonImages;
        } else
        {
            $this->images = json_encode([0 =>'no-image.png']);
        }
         $this->save();
    }

    public function updateImages($arrImages, $product)
    {
        if (is_array($arrImages))
        {
            $this->removeImages($product);
            $arrImageNames = [];
            $i             = 0;
            foreach ($arrImages as $image)
            {
                $fileName          = $product->id.'-'.$i.'.'.$image->extension();
                $image->move('uploads', $fileName);
                $arrImageNames[$i] = $fileName;
                $i++;
            }
            $jsonImages = json_encode($arrImageNames);
            $this->images = $jsonImages;
            $this->save();
        }
    }

    public function decodeJson($products)
    {
        if(count($products)>1)
        {
            foreach ($products as $product)
            {
                if($product->images != null)
                {
                   $product->images = json_decode($product->images); 
                } else
                {
                    $product->images = [0 =>'no-image.png'];
                }
            }
        } else
        {
//            if($products->images != null)
//                {
//                   $products->images = json_decode($products->images);
//                } else
//                {
//                    $products->images = [0 =>'no-image.png'];
//                }
        }
            
        return $products;
    }

    public function getXmlFromUrl($url)
        {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT,
                'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

            $xmlstr = curl_exec($ch);
            curl_close($ch);

            return $xmlstr;
        }

    public function updatePriceFromElektreka()
    {

        $xmlstr = self::getXmlFromUrl('https://elektreka.com.ua/price/yandex.xml');

        $xmlobj = new \SimpleXMLElement($xmlstr);

        $datas = (array)$xmlobj->shop->offers;
//        dd(count($datas['offer']));

        //Створюємо масив $arrCategory, в якиз записуємо всі категоріх та їх ID
        //Створюємо масив $arrSubcategory, в якиз записуємо всі субкатегоріх та їх ID
        $arrSubcategory = array();
        $i              = 0;
        $j              = 0;
        $k              = 0;

        $data = (array)$xmlobj->shop->categories;
        foreach ($xmlobj->shop->categories->category as $value) {
            if (count(get_object_vars($value)['@attributes']) == 1) {
                $arrCategory[$i]['id']       = get_object_vars($value)['@attributes']['id'];
                $arrCategory[$i]['category'] = $data['category'][$k];
                $i++;
            } else {
                $arrSubcategory[$j]['id']          = get_object_vars($value)['@attributes']['id'];
                $arrSubcategory[$j]['categoryId']  = get_object_vars($value)['@attributes']['parentId'];
                $arrSubcategory[$j]['subcategory'] = $data['category'][$k];
                $j++;
            }
            $k++;
        }
        
        //Записуємо категории и субкатегории в БД
        for ($i = 0; $i < count($arrCategory); $i++) {
            $id       = (int) $arrCategory[$i]['id'];
            $category = $arrCategory[$i]['category'];
            $slug = SlugService::createSlug(Product::class, 'slug', $category);

            $query  =  \DB::insert("INSERT IGNORE INTO categories (id, name, slug) VALUES ('$id', '$category', '$slug')");

        }

        for ($i = 0; $i < count($arrSubcategory); $i++) {
            $idCategories  = (int) $arrSubcategory[$i]['categoryId'];
            $idSubcategory = (int) $arrSubcategory[$i]['id'];
            $subcategory   = $arrSubcategory[$i]['subcategory'];
            $slug = SlugService::createSlug(Product::class, 'slug', $subcategory);

            $query  = \DB::insert("INSERT IGNORE INTO subcategories (id, categories_id, name, slug)"
                . " VALUES ('$idSubcategory', '$idCategories', '$subcategory', '$slug')");
            
        }
        
        //Записуємо в БД інформацію про товари
        $datas = (array)$xmlobj->shop->offers;
        
        foreach ($datas['offer'] as $data) {
            $data = (array)$data;
            $id                = (int) $data['@attributes']['id'];
            $name              = $data['name'];
            $slug = SlugService::createSlug(Product::class, 'slug', $name);
            $idSubCategories = (int) $data['categoryId'];
            $price             = (float) $data['price'];
            $vendor            = $data['vendor'];
            $description       = $data['description'];
//            $picture           = $data['picture'];

            if (is_array($data['picture'])) {

                $images = self::saveManyPics($data['picture'], $id);
            }
             else {
                $images = self::saveOnePic($data['picture'], $id);
            }
//            $images = json_encode([0 =>'no-image.png']);

            $query  = \Db::insert("INSERT INTO products (id, name, slug, categories_id, subcategories_id, price, vendor, description, images)
			 VALUES ('$id', '$name', '$slug',(SELECT categories_id FROM subcategories WHERE id='$idSubCategories'),
			 '$idSubCategories', '$price', '$vendor', '$description',  '$images')
			 ON DUPLICATE KEY UPDATE price='$price'");
        } 
    }
    
    public function saveOnePic($picsUrl, $id)
    {
        $image    = file_get_contents($picsUrl);
        $path = public_path('uploads/');
        $fileName = $id.'.jpg';
        file_put_contents($path.$fileName, $image);
        $arrImageNames[0] = $fileName;
        $jsonImages = json_encode($arrImageNames);
        return $jsonImages;
    }

    public function saveManyPics($arrPics, $id)
    {
        $i = 0;
        foreach ($arrPics as $picUrl) {
            $image    = file_get_contents($picUrl);
            $path = public_path('uploads/');
            $fileName = $id.'-'.$i.'.jpg';
            file_put_contents($path.$fileName, $image);
            $arrImageNames[$i] = $fileName;
            $i++;
        }   
        $jsonImages = json_encode($arrImageNames);
        return $jsonImages;
    }

    public function getPriceWithDiscount($products)
    {
        if(count($products) > 1)
        {
            foreach($products as $product)
            {
                $product->discountPrice = 0;
                if($product->discount > 0)
                {
                    $product->discountPrice = number_format(round($product->price - $product->price*$product->discount/100, 2), 2, ',', ' ');
                }
            }
        }else
        {
            $products->discountPrice = number_format(round($products->price - $products->price*$products->discount/100, 2), 2, ',', ' ');
        }
        
        return $products;
    }

    public function formatPrice($products)
    {
        if(count($products) > 1)
        {
            foreach($products as $product)
            {
                $product->price = number_format($product->price,  2, ',', ' ');
            }
        }else
        {
            $products->price = number_format($products->price, 2 , ',', ' ');
        }
        
        return $products;
    }
}
