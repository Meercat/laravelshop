<?php

namespace App\Providers;

use App\Category;
use App\Product;
use App\Subcategory;
use App\Cart;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('pages._menu', function($view)
        {
           $view->with('categories', Category::take(7)->get());
           $view->with('hiddenCategories', Category::skip(7)->take(1000)->get());
           $view->with('subcategories', Subcategory::all());
        });

        view()->composer('pages._sidebar', function($view)
        {
            $obj = new Product;
            /*
             * for GORIACHEE PREDLOGENIE slider
             */
           $hotDealsProductsWithJson = Product::where('hot_deals', 1)->get();

           /*
            * if hotDels Products isn't set
            * get rendom 6 items
            */
           if(count($hotDealsProductsWithJson) == 0)
           {
               $hotDealsProductsWithJson = Product::all()->random(6);
           }
           //formate price
           $hotDealsProductsWithJson = $obj->formatPrice($hotDealsProductsWithJson);
           $hotDealsProducts = $obj->decodeJson($hotDealsProductsWithJson);
           $hotDealsProducts = $obj->getPriceWithDiscount($hotDealsProducts);
           $view->with('hotDealsProducts', $hotDealsProducts);
           /*
            *for  TOP PRODAG slider
            */
           $bestSellerProductsWithJson = Product::where('bestsellers', 1)->get();
           /*
            * if bestSeller Products isn't set
            * get rendom 12 items
            */
           if(count($bestSellerProductsWithJson) == 0)
           {
               $bestSellerProductsWithJson = Product::all()->random(12);
           }
           //formate price
           $bestSellerProducts = $obj->formatPrice($bestSellerProductsWithJson);
           $bestSellerProducts = $obj->decodeJson($bestSellerProductsWithJson);
           $bestSellerProducts = $obj->getPriceWithDiscount($bestSellerProducts)->chunk(4);

           $view->with('bestSellerProducts', $bestSellerProducts);
        });

        view()->composer('layout', function($view)
        {
            $obj = new Cart;
            $countItems = $obj->getTotalItemsInCart();
           $view->with('countItems', $countItems);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
