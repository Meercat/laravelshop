<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Subcategory extends Model
{
    use Sluggable;

    const IS_NOT_DISPLAYED = 0;
    
    protected $fillable = ['categories_id', 'discount', 'is_displayed'];
    
    public function category()
    {
        return $this->belongsTo(Category::class, 'categories_id');
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
        public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public static function add($fields)
    {
        $subcategory = new static;
        $subcategory->isDisplayed($fields);
        $subcategory->name = $fields['name'];
        $subcategory->fill($fields);
        $subcategory->save();
    }

    public function edit($fields)
    {
        if($fields['name'] != null)
        {
            $this->slug = null;
            $this->name = $fields['name'];
        }

        $this->isDisplayed($fields);
        $this->fill($fields);
        $this->save();
    }

    public function isDisplayed($fields)
    {
        if(!array_key_exists('is_displayed', $fields))
        {
            $this->is_displayed = self::IS_NOT_DISPLAYED;
        }
    }
}
