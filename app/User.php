<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    const IS_ADMIN = 1;
    const IS_NOT_ADMIN = null;
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public static function create($fields)
    {
        $user = new static;
        
        if(!array_key_exists('is_admin', $fields))
        {
            $user->is_admin = self::IS_NOT_ADMIN;
        }
        $user->fill($fields);
        $user->password = bcrypt($fields['password']);
        $user->save();

        return $user;
    }

    public function edit($fields)
    {
        $this->fill($fields);
        if ($fields['password'] != null)
        {
            $this->password = bcrypt($fields['password']);
        }
        if(!array_key_exists('is_admin', $fields))
        {
            $this->is_admin = null;
        }
        $this->save();
    }

    public function remove($id)
    {
        $this->delete($id);
    }

    public function uploadAvatar($image)
    {
        if ($image == null)
        {
            return;
        }

        Storage::delete('uploads/avatars'.$this->image);
        $fileName    = str_random(10).'.'.$image->extention();
        $image->saveAs('uploads/avatars', $fileName);
        $this->image = $fileName;
    }
    
        public function getAvatar()
    {
        if($this->image == null)
        {
            return '/img/no-user-image.png';
        }
        return '/uploads' . $this->image;
    }

    public function makeAdmin()
    {
        $this->is_admin = User::IS_ADMIN;
    }

    public function makeNotAdmin()
    {
        $this->is_admin = User::IS_NOT_ADMIN;
    }

    public function toggleAdmin($vallue)
    {
        if($value == null)
        {
            return $this->makeAdmin();
        }

        return $this->makeNotAdmin();
    }
}
