<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->integer('categories_id');
            $table->integer('subcategories_id');
            $table->double('price', 10, 2);
            $table->integer('discount')->default(0);;
            $table->string('unite_of_measure');
            $table->string('vendor');
            $table->boolean('available');
            $table->integer('recommended')->default(0);
            $table->integer('is_displayed')->default(1);
            $table->integer('hot_deals')->default(null);
            $table->integer('bestsellers')->default(null);
            $table->text('description');
            $table->string('images')->default('no_image.png');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
