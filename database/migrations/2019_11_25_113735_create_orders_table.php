<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status')->defaulte('new');
            $table->integer('users_id');
            $table->string('name');
            $table->string('second_name');
            $table->string('surname');
            $table->integer('phone');
            $table->string('email')->nullable();
            $table->string('delivery');
            $table->string('address');
            $table->string('user_comments')->nullable();
            $table->string('admin_comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
