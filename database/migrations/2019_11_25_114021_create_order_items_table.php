<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderItems', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orders_id');
            $table->integer('products_id');
            $table->float('price');
            $table->integer('quantity');
            $table->integer('product_discount')->default(0);
            $table->integer('category_discount')->default(0);
            $table->integer('subcategory_discount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
