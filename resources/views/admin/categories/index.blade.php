@extends('admin.layout.layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Категории товаров
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box">
              
            <div class="box-header"> 
              <h3 class="box-title">Главная категорий товаров</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                <a href="{{route('categories.create')}}" class="btn btn-success btn-lg">Добавить категорию</a>
                </div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Категория</th>
                  <th>Скидка по категории (%)</th>
                  <th>Отображать/скрить</th>
                  <th>Редактировать</th>
                  <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($categories as $category)
                    <tr>
                        <td>{{$category->id}}</td>
                        <td>{{$category->name}}</td>
                        <td>{{$category->discount}}</td>
                        @if($category->is_displayed == true)
                        <td><li class="fa fa-eye"></li></td>
                        @else
                        <td><li class="fa fa-eye-slash"></li></td>
                        @endif
                        <td><a href="{{route('categories.edit', $category->id)}}" class="fa fa-edit"></a></td>
                        <td>
                        {!! Form::open(['route' => ['categories.destroy', $category->id], 'method' => 'delete']) !!}
                        <button onclick="return confirm('Вы уверены?')" type="submit">
                            <i class="fa fa-remove"></i>
                        </button>
                        {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @endsection