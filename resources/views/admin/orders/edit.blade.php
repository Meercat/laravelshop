@extends('admin.layout.layout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Редактирование заказов
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Редактирование заказа</h3>
                        @include('admin.errors')
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['route' => ['orders.update', $order->id], 'id' => 'update', 'method' => 'put']) !!}
                    <div class="box-body">
                        <div class="box-header with-border">
                            <h3 class="box-title">Получатель</h3>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Фамилия</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name='surname' value="{{$order->surname}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Имя</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name='name' value="{{$order->name}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Отчество</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name='second_name' value="{{$order->second_name}}">
                        </div>
                        <div class="box-header with-border">
                            <h3 class="box-title">Контактный телефон</h3>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Телефон</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name='phone' value="{{$order->phone}}">
                        </div>
                        <div class="box-header with-border">
                            <h3 class="box-title">Адрес доставки</h3>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Адрес</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name='address' value="{{$order->address}}">
                        </div>
                        <div class="box-header with-border">
                            <h3 class="box-title">Список товаров</h3>
                        </div>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Название товара</th>
                                    <th>Колличество</th>
                                    <th>Удалить</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($order->orderItems as $item)
                                <tr>
                                    <td>
                                        <input type="text" class="form-control" id="exampleInputEmail1" name='product_name' value="{{$item->product->name}}" disabled></td>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="exampleInputEmail1" name='quantity' value="{{$item->quantity}}" disabled></td>
                                    <td>
                                        <button type="button" class="remove_button" value="{{$item->id}}">
                                            <i class="fa fa-remove"></i>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <div class="box-header with-border">
                            <h3 class="box-title">Статус заказа</h3>
                        </div>
                        <div class="form-group">
                            <select class="form-control select2"  name="status" style="width: 100%;">
                                <option value='new' selected>новый</option>
                                <option value='confirmed'>заказ подтвержден</option>
                                <option value='prepare to sent'>формерование заказа</option>
                                <option value='rady to sent'>готов к отправке</option>
                                <option value='sended'>отправлен</option>
                                <option value='confirmed reciving'>подтверждено получение</option>
                                <option value='paid'>оплачен</option>
                                <option value='successfully closed'>успешно закрыт</option>
                            </select>
                        </div>
                        <div class="box-body pad">
                            <label for="exampleInputPassword1">Комменратий админа</label>
                            <textarea id="editor1" name="admin_comments" rows="10" cols="80"></textarea>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" form="update" class="btn btn-primary">Изменить</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-body">
                        {!! Form::open(['route' => ['orders.addNewItemToOrder', $order->id], 'id' => 'addNewItemToOrder']) !!}
                        <div class="box-header with-border">
                            <h3 class="box-title">Добавить товар</h3>
                        </div>
                        <div class="form-group">
                            <select class="form-control select2"  name="product_id" style="width: 100%;">
                                @foreach($products as $product)
                                <option value="{{$product->id}}">
                                    {{$product->name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Количество</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name='quantity'>
                        </div>
                        <div class="box-footer">
                            <button type="submit" form="addNewItemToOrder" class="btn btn-primary">Добавить</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection