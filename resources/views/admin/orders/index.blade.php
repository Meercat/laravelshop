@extends('admin.layout.layout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Заказы
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Главная заказы</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="overflow-x:scroll">
                        <div class="form-group">
                            <a href="{{route('orders.index')}}" class="btn btn-success btn-lg">Создать заказ (пока перенаправляет на админку список заказов)</a>
                        </div>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Номер заказа</th>
                                    <th>Статус за</th>
                                    <th>Получатель</th>
                                    <th>Список товаров</th>
                                    <th>Цена</th>
                                    <th>Скидка</th>
                                    <th>Общая стоимость</th>
                                    <th>Контактный телефон</th>
                                    <th>Адрес доставки</th>
                                    <th>Комментарий юзера</th>
                                    <th>Комментарий админа</th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <th>{{$order->id}}</th>
                                    <th>{{$order->status}}</th>
                                    <th>{{$order->surname}} {{$order->name}} {{$order->second_name}}</th>
                                    <th>
                                        @foreach($order->OrderItems as $orderItem)
                                        {{$orderItem->product->name}}<br/>
                                        @endforeach
                                    </th>
                                    <th>
                                        @foreach($order->OrderItems as $orderItem)
                                        {{$orderItem->price}}<br/>
                                        @endforeach
                                    </th>
                                    <th>
                                        @foreach($order->OrderItems as $orderItem)
                                        {{$orderItem->product->discount}}<br/>
                                        @endforeach
                                    </th>
                                    <th>{{$order->orderPrice}}</th>
                                    <th>{{$order->phone}}</th>
                                    <th>{{$order->address}}</th>
                                    <th>{{$order->user_comments}}</th>
                                    <th>{!! $order->admin_comments !!}</th>
                                    <th><a href="{{route('orders.edit', $order->id)}}" class="fa fa-edit"></th>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection