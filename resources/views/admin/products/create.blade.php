@extends('admin.layout.layout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Добавление новых товаров
        </h1>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Создание товара</h3>
                        @include('admin.errors')
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['route' => ['products.store'], 'files' => true]) !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Название товара</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name='name' value="{{old('name')}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Название категории</label>
                            <select class="form-control" name="categories_id">
                                @foreach($categories as $category)
                                <option value="{{$category->id}}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Название субкатегории</label>
                            <select class="form-control" name="subcategories_id">
                                @foreach($subcategories as $subcategory)
                                <option value="{{$subcategory->id}}">{{ $subcategory->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Цена (грн.)</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" name="price"  value="{{old('price')}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Скидка (%)</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" name="discount" value="0"  value="{{old('discount')}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Единицы измерения</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" name="unite_of_measure"  value="{{old('unite_of_measure')}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Производитель</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" name="vendor"  value="{{old('vendor')}}">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="available" value="1">
                                В наличии
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="recommended" value="1">
                                Рекомендованный товар
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_displayed" value="1">
                                Отображать товар
                            </label>
                        </div
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="hot_deals" value="1">
                                Горячее предложение
                            </label>
                        </div
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="bestsellers" value="1">
                                TOP продаж
                            </label>
                        </div
                        <div class="form-group">
                            <label for="exampleInputFile">Загрузить картинки</label>
                            <input type="file" name="images[]" multiple  id="exampleInputFile">
                        </div>
                        <div class="box-body pad">
                            <label for="exampleInputPassword1">Описание товара</label>
                            <textarea id="editor1" name="description" rows="10" cols="80">{{old('description')}}</textarea>
                        </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Добавить</button>
                    </div>
                    {!! Form::close() !!}
                    <!-- form end -->
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection