    @extends('admin.layout.layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Редактирование товаров
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Редактирование товара</h3>
                        @include('admin.errors')
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['route' => ['products.update', $product->id], 'method' => 'put', 'files' => true]) !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Название товара</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name='name' value="{{$product->name}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Название категории</label>
                            <select class="form-control" name="categories_id">
                                @foreach($categories as $category)
                                <option <?php if($product->category->id == $category->id) echo 'selected' ?>
                                    value="{{$category->id}}">
                                    {{ $category->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Название субкатегории</label>
                            <select class="form-control" name="subcategories_id">
                                @foreach($subcategories as $subcategory)
                                <option <?php if($product->subcategory->id == $subcategory->id) echo 'selected' ?>
                                    value="{{$subcategory->id}}">
                                    {{ $subcategory->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Цена (грн.)</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" name="price" value="{{$product->price}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Скидка (%)</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" name="discount" value="{{$product->discount}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Единицы измерения</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" name="unite_of_measure" value="{{$product->unite_of_measure}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Производитель</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" name="vendor" value="{{$product->vendor}}">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="available" <?php if ($product->available) echo 'checked' ?> value="1">
                                В наличии
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="recommended" <?php if ($product->recommended) echo 'checked' ?> value="1">
                                Рекомендованный
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_displayed" <?php if ($product->is_displayed) echo 'checked' ?> value="1">
                                Отображать товар
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="hot_deals" <?php if ($product->hot_deals) echo 'checked' ?> value="1">
                                Горячее предложение
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="bestsellers" <?php if ($product->bestsellers) echo 'checked' ?> value="1">
                                TOP продаж
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Загрузить новые картинки
                                <p class="help-block">(при загрузке новый картинок <br/>существующие будут удалены)</p>
                            </label>
                            <input type="file" name="images[]" multiple  id="exampleInputFile">
                        </div>
                        <div class="box-body pad">
                            <label for="exampleInputPassword1">Описание товара</label>
                            <textarea id="editor1" name="description" rows="10" cols="80">{!! $product->description !!}</textarea>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Изменить</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @endsection