@extends('admin.layout.layout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Товары
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <!-- /.box -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Главная товаров</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="overflow-x:scroll">
                        <div class="form-group">
                            <a href="{{route('products.create')}}" class="btn btn-success btn-lg">Добавить товар</a>
                        </div>
                        <div class="form-group">
                            <a href="{{route('products.updatePriceFromElektreka')}}" class="btn btn-success btn-lg">Обновить прайс с Еlektrek</a>
                        </div>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Товар</th>
                                    <th>Категория</th>
                                    <th>Субкатегория</th>
                                    <th>Цена</th>
                                    <th>Скидка(%)</th>
                                    <th>Ед. изм.</th>
                                    <th>Производитель</th>
                                    <th>В наличии</th>
                                    <th>Рекоменд.</th>
                                    <th>Отображать</th>
                                    <th>Hot diels</th>
                                    <th>best seller</th>
                                    <!--<th>Описание</th>-->
                                    <th>Картинки</th>
                  <!--                  <th> </th>
                                    <th> </th>-->
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                <tr>
                                    <td>{{$product->id}}</td>
                                    <td>{{$product->name}}</td>
                                    <td>{{$product->category->name}}</td>
                                    <td>{{$product->subcategory->name}}</td>
                                    <td>{{$product->price}}</td>
                                    <td>{{$product->discount}}</td>
                                    <td>{{$product->unite_of_measure}}</td>
                                    <td>{{$product->vendor}}</td>

                                    @if($product->available == true)
                                    <td><li class="fa fa-unlock"></li></td>
                            @else
                            <td><li class="fa  fa-lock"></li></td>
                            @endif

                            @if($product->recommended == true)
                            <td><li class="fa fa-thumbs-o-up"></li></td>
                            @else
                            <td><li class="fa fa-thumbs-o-down"></li></td>
                            @endif

                            @if($product->is_displayed == true)
                            <td><li class="fa fa-eye"></li></td>
                            @else
                            <td><li class="fa fa-eye-slash"></li></td>
                            @endif

                            @if($product->hot_deals == true)
                            <td>Да</td>
                            @else
                            <td>Нет</li></td>
                            @endif

                            @if($product->bestsellers == true)
                            <td>Да</td>
                            @else
                            <td>Нет</td>
                            @endif
                            <!--<td>{!! $product->description !!}</td>-->
                            <td>
                                @foreach($product->images as $image)
                                <div class="col-sm-6">
                                    <img src="{{ asset('uploads/'.$image) }}" class="img-responsive">
                                    <br/>
                                </div>
                                @endforeach
                            </td>
                            <td><a href="{{route('products.edit', $product->id)}}" <li class="fa fa-edit"></a></li></td>
                            <td>
                                {!! Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete']) !!}
                                <button onclick="return confirm('Вы уверены?')" type="submit">
                                    <i class="fa fa-remove"></i>
                                </button>
                                {!! Form::close() !!}
                            </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- Start Pagination Area -->
                        <div class="pagination-area">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="pagination">
                                        <ul>
                                            {{$products->render()}}
                                        </ul>
                                    </div>
                                </div>
<!--                                <div class="col-xs-7">
                                    <div class="product-result">
                                        <span>Showing 1 to 16 of {!! $products->total() !!} ({!! $products->lastPage()!!} Pages)</span>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                        <!-- End Pagination Area -->
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection