@extends('admin.layout.layout')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Редактирование субкатегории товаров
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Редактирование субкатегории</h3>
                        @include('admin.errors')
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    {!! Form::open(['route' => ['subcategories.update', $subcategory->id], 'method' => 'put']) !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Название субкатегории</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name='name' placeholder="{{$subcategory->name}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Название категории</label>
                            <select class="form-control" name="categories_id">
                                @foreach($categories as $category)
                                <option <?php if($subcategory->categories->id == $category->id) echo 'selected' ?>
                                    value="{{$category->id}}">
                                    {{ $category->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Скидка по субкатегории (%)</label>
                            <input type="text" class="form-control" id="exampleInputPassword1" name='discount' value="{{$subcategory->discount}}">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_displayed" <?php if ($subcategory->is_displayed) echo 'checked' ?> value="1">
                                Отображать субкатегорию
                            </label>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Изменить</button>
                    </div>
                    <!--{!! Form::close() !!}-->
                </div>
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection