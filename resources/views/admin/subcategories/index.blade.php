@extends('admin.layout.layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
     <!--Content Header (Page header)-->
    <section class="content-header">
      <h1>
        Субкатегории
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Главная субкатегорий</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                <a href="{{route('subcategories.create')}}" class="btn btn-success btn-lg">Добавить субкатегорию</a>
                </div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Субкатегория</th>
                  <th>Категория</th>
                  <th>Скидка по субкатегории (%)</th>
                  <th>Отображать/скрить</th>
                  <th>Редактировать</th>
                  <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($subcategories as $subcategory)
                    <tr>
                        <td>{{$subcategory->id}}</td>
                        <td>{{$subcategory->name}}</td>
                        <td>{{$subcategory->category->name}}</td>
                        <td>{{$subcategory->discount}}</td>
                        @if($subcategory->is_displayed == true)
                        <td><li class="fa fa-eye"></li></td>
                        @else
                        <td><li class="fa fa-eye-slash"></li></td>
                        @endif
                        <td><a href="{{route('subcategories.edit', $subcategory->id)}}" class="fa fa-edit"></a></td>
                        <td>
                        {!! Form::open(['route' => ['subcategories.destroy', $subcategory->id], 'method' => 'delete']) !!}
                        <button onclick="return confirm('Вы уверены?')" type="submit">
                            <i class="fa fa-remove"></i>
                        </button>
                        {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @endsection