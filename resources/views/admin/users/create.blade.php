@extends('admin.layout.layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          Создание нового пользователя
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Создание пользователя</h3>
                  @include('admin.errors')
              </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'users.store']) !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Имя пользователя</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name='name' value="{{old('name')}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">email</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name='email' value="{{old('email')}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Пароль</label>
                    <input type="password" class="form-control" id="exampleInputEmail1" name='password' value="{{old('password')}}">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name='is_admin' value="1"> Сделать адміністратором
                    </label>
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Добавить</button>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @endsection