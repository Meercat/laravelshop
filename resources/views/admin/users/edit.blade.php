@extends('admin.layout.layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Редактирование категории товаров
      </h1>
    </section>
        <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
              <div class="box-header with-border">
                  <h3 class="box-title">Редактирование категории</h3>
                  @include('admin.errors')
              </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => ['users.update', $user->id], 'method' => 'put']) !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Имя пользователя</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name='name' value="{{$user->name}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">email</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name='email' value="{{$user->email}}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Новий пароль</label>
                    <br/>
                    <small>Если поле оставить пустым, пароль останется прежним</small>
                    <input type="password" class="form-control" id="exampleInputEmail1" name='password'>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name='is_admin' <?php if($user->is_admin == 1) {echo "checked";} ?> value="1"> Сделать адміністратором
                    </label>
                </div>
            </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Изменить</button>
              </div>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @endsection