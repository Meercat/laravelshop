@extends('admin.layout.layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Пользователи
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Главная пользователи</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="overflow-x:scroll">
                <div class="form-group">
                <a href="{{route('users.create')}}" class="btn btn-success btn-lg">Добавить нового пользователя</a>
                </div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Имя</th>
                  <th>Отчество</th>
                  <th>Фамилия</th>
                  <th>email</th>
                  <th>телефон</th>
                  <th>адміністратор</th>
                  <th> </th>
                  <th> </th>
                </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->second_name}}</td>
                        <td>{{$user->surname}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->phone}}</td>
                        @if($user->is_admin == true)
                        <td> <li class="fa  fa-user-secret"></li><label for="exampleInputEmail1">Admin</label></td>
                        @else
                        <td>User</td>
                        @endif
                        <td><a href="{{route('users.edit', $user->id)}}" class="fa fa-edit"></a></td>
                        <td>
                        {!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
                        <button onclick="return confirm('Вы уверены?')" type="submit">
                            <i class="fa fa-remove"></i>
                        </button>
                        {!! Form::close() !!}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 @endsection