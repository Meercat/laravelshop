<!-- CATEGORY-MENU-LIST START -->
<div class="left-category-menu hidden-sm hidden-xs">
    <div class="left-product-cat">
        <div class="category-heading">
            <h2>Действия</h2>
        </div>
        <div class="category-menu-list">
            <ul>
                <!-- Single menu start -->
                <li>
                    <a href="{{asset(route('cabinets.index'))}}">Мои текущие заказы</a>
                    <a href="{{asset(route('cabinets.getAllOrders'))}}">Все мои заказы</a>
                    <!--<a href="#">Редактировать мои данные</a>-->
                    <!--  MEGA MENU START -->
                    <!-- MEGA MENU END -->
                </li>
                <!-- Single menu end -->
                <!-- MENU ACCORDION START -->
            </ul>
        </div>
    </div>
</div>
<!-- END CATEGORY-MENU-LIST -->
