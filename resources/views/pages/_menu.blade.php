<!-- CATEGORY-MENU-LIST START -->
<div class="left-category-menu hidden-sm hidden-xs">
    <div class="left-product-cat">
        <div class="category-heading">
            <h2>Категории</h2>
        </div>
        <div class="category-menu-list">
            <ul>
                <!-- Single menu start -->
                @foreach ($categories as $category)
                <li class="arrow-plus">
                    <a href="{{route('getProductsOfCategory', [7, $category->slug])}}">{{$category->name}}</a>
                    <!--  MEGA MENU START -->
                    <div class="cat-left-drop-menu">
                        <div class="cat-left-drop-menu-left">
                            <!--<a class="menu-item-heading" href="#">Handbags</a>-->
                            <ul>
                                @foreach($subcategories as $subcategory)
                                @if($category->id == $subcategory->categories_id)
                                <li><a href="{{route('getProductsOfSubcategory', [7, $subcategory->slug])}}">
                                        {{$subcategory->name}}
                                    </a></li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- MEGA MENU END -->
                </li>
                @endforeach
                <!-- Single menu end -->
                <!-- MENU ACCORDION START -->
                @foreach ($hiddenCategories as $hiddenCategory)
                <li class="arrow-plus rx-child">
                    <a href="{{route('getProductsOfCategory', [7, $hiddenCategory->slug])}}">{{$hiddenCategory->name}}</a>
                    <!--  MEGA MENU START -->
                    <div class="cat-left-drop-menu">
                        <div class="cat-left-drop-menu-left">
                            <!--<a class="menu-item-heading" href="#">Handbags</a>-->
                            <ul>
                                @foreach($subcategories as $subcategory)
                                @if($hiddenCategory->id == $subcategory->categories_id)
                                <li><a href="{{route('getProductsOfSubcategory', [7, $subcategory->slug])}}">
                                        {{$subcategory->name}}
                                    </a></li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <!-- MEGA MENU END -->
                </li>
                @endforeach
                <li class="rx-parent">
                    <a class="rx-default">
                        Больше категорий <span class="cat-thumb  fa fa-plus"></span>
                    </a>
                    <a class="rx-show">
                        Скрыть <span class="cat-thumb  fa fa-minus"></span>
                    </a>
                </li>
                <!-- MENU ACCORDION END -->
            </ul>
        </div>
    </div>
</div>
<!-- END CATEGORY-MENU-LIST -->
