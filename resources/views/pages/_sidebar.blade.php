<!-- START HOT-DEALS-AREA -->
<div class="hot-deals-area carosel-circle">
    <div class="row">
        <div class="col-md-12">
            <div class="area-title">
                <h3 class="title-group border-red gfont-1">Горячее предложение</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="active-hot-deals">
            <!-- Start Single-hot-deals -->
            @foreach($hotDealsProducts as $hotDeal)
            <div class="col-xs-12">
                <div class="single-hot-deals">
                    <div class="hot-deals-photo">
                        <a href="{{route('getProductDetails', $hotDeal->slug)}}"><img src="{{asset('uploads/'.$hotDeal->images[0])}}" alt="Product"></a>
                    </div>
                    <div class="count-down">
                        <div class="timer">
                            <div data-countdown="2017/12/31"></div>
                        </div>
                    </div>
                    <div class="hot-deals-text">
                        <h5><a href="{{route('getProductDetails', $hotDeal->slug)}}" class="name-group">{{$hotDeal->name}}</a></h5>
                        <span class="rating"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i></span>
                        <div class="price-box">
                            @if($hotDeal->discount !=0)
                            <span class="price gfont-2">{{$hotDeal->discountPrice}}</span>
                            <span class="old-price gfont-2">{{$hotDeal->price}}</span>
                            @else
                            <span class="price gfont-2">{{$hotDeal->price}}</span>
                            @endif
                        </div>
                    </div>
                </div>

            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- END HOT-DEALS-AREA -->
<!-- START SMALL-PRODUCT-AREA -->
<div class="small-product-area carosel-navigation">
    <div class="row">
        <div class="col-md-12">
            <div class="area-title">
                <h3 class="title-group gfont-1">TOP Продаж</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="active-bestseller sidebar">
            @foreach($bestSellerProducts as $bestSeller)

            <div class="col-xs-12">
                <!-- Start Single-Product -->
                @foreach($bestSeller as $product)
                <div class="single-product">
                    <div class="product-img">
                        <a href="{{route('getProductDetails', $product->slug)}}">
                            <img class="primary-img" src="{{asset('uploads/'.$product->images[0])}}" alt="Product">
                        </a>
                    </div>
                    <div class="product-description">
                        <h6><a href="{{route('getProductDetails', $product->slug)}}">{{$product->name}}</a></h6>
                        <div class="price-box">
                            @if($product->discount !=0)
                            <span class="price">{{$product->discountPrice}}</span>
                            <span class="old-price">{{$product->price}}</span>
                            @else
                            <span class="price">{{$product->price}}</span>
                            @endif
                            <!--<span class="price">{{$product->price}}</span>-->
                            <!--<span class="old-price">$120.00</span>-->
                        </div>
                        <span class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                        </span>
                    </div>
                </div>
                @endforeach
                <!-- End Single-Product -->
            </div>
            @endforeach
        </div>
    </div>



</div>
<!-- END SMALL-PRODUCT-AREA -->
<!-- START SIDEBAR-BANNER -->
<!--    <div class="sidebar-banner">
        <div class="active-sidebar-banner">
            <div class="single-sidebar-banner">
                <a href="#"><img src="{{asset('front/img/banner/1.jpg')}}" alt="Product Banner"></a>
            </div>
            <div class="single-sidebar-banner">
                <a href="#"><img src="{{asset('front/img/banner/2.jpg')}}" alt="Product Banner"></a>
            </div>
        </div>
    </div>-->
<!-- END SIDEBAR-BANNER -->
<!-- START RECENT-POSTS -->
<!--    <div class="shop-blog-area sidebar">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title-group border-red gfont-1">RECENT POSTS </h3>
            </div>
        </div>
        <div class="row">
            <div class="active-recent-posts carosel-circle">
                 Start Single-Recent-Posts 
                <div class="col-xs-12">
                    <div class="single-recent-posts">
                        <div class="recent-posts-photo">
                            <img src="{{asset('front/img/posts/1.jpg')}}" alt="Recent Posts">
                        </div>
                        <div class="recent-posts-text">
                            <h5><a href="#" class="recent-posts-title">swimwear for women</a></h5>
                            <span class="recent-posts-date">23/12/2015 | BootExpert Theme</span>
                            <p class="posts-short-brif">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                            <a href="#" class="posts-read-more">Read more ...</a>
                        </div>
                    </div>
                </div>
                 End Single-Recent-Posts 
                 Start Single-Recent-Posts 
                <div class="col-xs-12">
                    <div class="single-recent-posts">
                        <div class="recent-posts-photo">
                            <img src="{{asset('front/img/posts/2.jpg')}}" alt="Recent Posts">
                        </div>
                        <div class="recent-posts-text">
                            <h5><a href="#" class="recent-posts-title">swimwear for women</a></h5>
                            <span class="recent-posts-date">23/12/2015 | BootExpert Theme</span>
                            <p class="posts-short-brif">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                            <a href="#" class="posts-read-more">Read more ...</a>
                        </div>
                    </div>
                </div>
                 End Single-Recent-Posts 
                 Start Single-Recent-Posts 
                <div class="col-xs-12">
                    <div class="single-recent-posts">
                        <div class="recent-posts-photo">
                            <img src="{{asset('front/img/posts/3.jpg')}}" alt="Recent Posts">
                        </div>
                        <div class="recent-posts-text">
                            <h5><a href="#" class="recent-posts-title">swimwear for women</a></h5>
                            <span class="recent-posts-date">23/12/2015 | BootExpert Theme</span>
                            <p class="posts-short-brif">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                            <a href="#" class="posts-read-more">Read more ...</a>
                        </div>
                    </div>
                </div>
                 End Single-Recent-Posts 
                 Start Single-Recent-Posts 
                <div class="col-xs-12">
                    <div class="single-recent-posts">
                        <div class="recent-posts-photo">
                            <img src="{{asset('front/img/posts/4.jpg')}}" alt="Recent Posts">
                        </div>
                        <div class="recent-posts-text">
                            <h5><a href="#" class="recent-posts-title">swimwear for women</a></h5>
                            <span class="recent-posts-date">23/12/2015 | BootExpert Theme</span>
                            <p class="posts-short-brif">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                            <a href="#" class="posts-read-more">Read more ...</a>
                        </div>
                    </div>
                </div>
                 End Single-Recent-Posts 
                 Start Single-Recent-Posts 
                <div class="col-xs-12">
                    <div class="single-recent-posts">
                        <div class="recent-posts-photo">
                            <img src="{{asset('front/img/posts/1.jpg')}}" alt="Recent Posts">
                        </div>
                        <div class="recent-posts-text">
                            <h5><a href="#" class="recent-posts-title">swimwear for women</a></h5>
                            <span class="recent-posts-date">23/12/2015 | BootExpert Theme</span>
                            <p class="posts-short-brif">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                            <a href="#" class="posts-read-more">Read more ...</a>
                        </div>
                    </div>
                </div>
                 End Single-Recent-Posts 
            </div>
        </div>
    </div>-->
<!-- END RECENT-POSTS -->