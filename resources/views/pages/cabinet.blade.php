@extends('layout')

@section('content')
<!-- START PAGE-CONTENT -->
<section class="page-content">
    <div class="container">
<!--        <div class="row">
            <div class="col-md-12">
                <ul class="page-menu">
                    <li><a href="index.html">Home</a></li>
                    <li class="active"><a href="#">Shopping Cart</a></li>
                </ul>
            </div>
        </div>-->
        <div class="row">
            <div class="col-md-3">
                <!-- CATEGORY-MENU-LIST START -->
                @include('pages._cabinet-menu')
                <!-- END CATEGORY-MENU-LIST -->
            </div>
            <div class="col-md-9">
                <!-- Start Shopping-Cart -->
                <div class="shopping-cart">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cart-title">
                                <h2 class="title-group-3 gfont-1">Покупки</h2>
                                <hr>
                            </div>
                            <!-- Start Table -->
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td class="text-center">№ заказа</td>
                                            <td class="text-left">Товары</td>
                                            <td class="text-left">Общая <br/> стоимость</td>
                                            <td class="text-right">Статус <br/> заказа</td>
                                            <td class="text-right">Дата <br/> заказа</td>
                                            <td class="text-right">Детали <br/> заказа</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($orders == null)
                                            <h2 class="entry-title">Вы еще не делали покупки.</h2>
                                            @else
                                            @foreach($orders as $order)
                                        <tr>
                                            <td class="text-center">
                                                {{$order->id}}
                                            </td>
                                            <td class="text-left">
                                                @foreach($order->orderItems as $item)
                                                {{$item->product->name}} - {{$item->quantity}} ед.
                                                <hr>
                                                @endforeach
                                            </td>
                                            <td class="text-center">
                                                {{$order->totalPrice}} грн.
                                            </td>
                                            <td class="text-right">{{$order->status}}</td>
                                            <td class="text-right">{{$order->created_at}}</td>
                                            <td class="text-center">
                                                <a href="/cabinet/order/{{$order->id}}"><li class=" fa fa-eye"></li></a>
                                            </td>
                                            @endforeach
                                            @endif
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- End Table -->
                            <!-- Start Pagination Area -->
                            <div class="pagination-area">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="pagination">
                                            <ul>
                                                {{$orders->render()}}
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Pagination Area -->
                        </div>
                    </div>
                </div>
                <!-- End Shopping-Cart -->
            </div>
        </div>
    </div>
    <!-- START BRAND-LOGO-AREA -->
<!--    <div class="brand-logo-area carosel-navigation">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="area-title">
                        <h3 class="title-group border-red gfont-1">Brand Logo</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="active-brand-logo">
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/4.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/5.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/6.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <!-- END BRAND-LOGO-AREA -->
    <!-- START SUBSCRIBE-AREA -->
    <div class="subscribe-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <label class="hidden-sm hidden-xs">Sign Up for Our Newsletter:</label>
                    <div class="subscribe">
                        <form action="#">
                            <input type="text" placeholder="Enter Your E-mail">
                            <button type="submit">Subscribe</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="social-media">
                        <a href="#"><i class="fa fa-facebook fb"></i></a>
                        <a href="#"><i class="fa fa-google-plus gp"></i></a>
                        <a href="#"><i class="fa fa-twitter tt"></i></a>
                        <a href="#"><i class="fa fa-youtube yt"></i></a>
                        <a href="#"><i class="fa fa-linkedin li"></i></a>
                        <a href="#"><i class="fa fa-rss rs"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SUBSCRIBE-AREA -->
</section>
<!-- END PAGE-CONTENT -->
@endsection