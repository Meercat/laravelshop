@extends('layout')

@section('content')
<!-- START PAGE-CONTENT -->
<section class="page-content">
    <div class="container">
<!--        <div class="row">
            <div class="col-md-12">
                <ul class="page-menu">
                    <li><a href="index.html">Home</a></li>
                    <li class="active"><a href="#">Shopping Cart</a></li>
                </ul>
            </div>
        </div>-->
        <div class="row">
            <div class="col-md-3">
                <!-- CATEGORY-MENU-LIST START -->
                @include('pages._menu')
                <!-- END CATEGORY-MENU-LIST -->
                <!-- START SMALL-PRODUCT-AREA -->
                @include('pages._sidebar')
                <!-- END SMALL-PRODUCT-AREA -->
            </div>
            <div class="col-md-9">
                <!-- Start Shopping-Cart -->
                <div class="shopping-cart">
                    <div class="row">
                        <div class="col-md-12">
                            <!--<div class="cart-title">-->
                                <h2 class="title-group-3 gfont-1">Корзина</h2>
                                @include('admin.errors')
                                <hr>
                            <!--</div>-->
                            <!-- Start Table -->
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td class="text-center">Товар</td>
                                            <td class="text-left">Название</td>
                                            <td class="text-left">Количество</td>
                                            <td class="text-right">Цена за единицу товара</td>
                                            <td class="text-right">Общая стоимость</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($items == null)
                                            <h3 class="title-group-3 gfont-1">В корзине нет товара</h3>
                                            @else
                                            @foreach($items as $item)
                                        <tr> 
                                            <td class="text-center">
                                                <a href="{{route('getProductDetails', $item->slug)}}"><img class="img-thumbnail" src="{{asset('uploads/' . $item->images[0])}}" alt="{{$item->name}}" /></a>
                                            </td>
                                            <td class="text-left">
                                                <a href="{{route('getProductDetails', $item->slug)}}">{{$item->name}}</a>
                                            </td>
                                            <td class="text-left">
                                                <div class="btn-block cart-put">
                                                    {!! Form::open(['route' => ['carts.updateItemQuantityInCart', $item->id], 'id' => $item->id]) !!}
                                                    <input  name="quantity" class="form-control" type="number"  min="0" max="1000" value="{{$item->quantity}}"/>
                                                    {!! Form::close() !!}
                                                    <div class="input-group-btn cart-buttons">
                                                        {!! Form::open(['route' => ['carts.removeItemInCart', $item->id], 'method' => 'delete']) !!}
                                                        <button class="btn btn-primary" form="{{$item->id}}" data-toggle="tooltip" title="Обновить">
                                                            <i class="fa fa-refresh"></i>
                                                        <button class="btn btn-danger" onclick="return confirm('Позиция будет удалена!!! Продолжить?')" data-toggle="tooltip" title="Удалить">
                                                            <i class="fa fa-times-circle"></i>
                                                        </button>
                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="text-right">{{$item->price}}</td>
                                            <td class="text-right">{{$item->price * $item->quantity}}</td>
                                            @endforeach
                                            @endif
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- End Table -->
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-8">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td class="text-right">
                                                    <strong>К оплате:</strong>
                                                </td>
                                                <td class="text-right">{{$totalPrice}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="shopping-checkout">
                                <a href="{{Redirect::back()->getTargetUrl()}}" class="btn btn-primary pull-left">Продолжить покупки</a>
                                <a href="{{asset(route('carts.removeAllItems'))}}" onclick="return confirm('Корзина будет очищена!!! Продолжить?')" class="btn btn-primary pull-right">Очистить корзину</a>
                            </div>
                            <!-- Accordion start -->
                            <hr>
                            <div class="accordion-cart">
                                <div class="panel-group" id="accordion">
                                    <!-- Start Shipping -->
                                    <div class="panel panel_default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-trigger" data-toggle="collapse" data-parent="#accordion" href="#shipping">Введите данные для оформления заказа</a>
                                            </h4>
                                        </div>
                                        {!! Form::open(['route' => 'orders.addNewOrder', 'id' => 'deliveryData']) !!}
                                        <div id="shipping" class="collapse in">
                                            <div class="panel-body">
                                                <div class="col-sm-12">
                                                    <p>Данные получателя</p>
                                                </div>
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"><sup>*</sup>Фамилия</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="surname" value="{{old('surname')}}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"><sup>*</sup>Имя</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="name" value="{{old('name')}}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"><sup>*</sup>Отчество</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="second_name" value="{{old('second_name')}}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"><sup>*</sup>Контактный телефон</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="phone" value="{{old('phone')}}" placeholder="(067)111-1111"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel-body">
                                                <div class="col-sm-12">
                                                    <p>Данные для отправки заказа</p>
                                                </div>
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"><sup>*</sup>Служба доставки</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="delivery" value="{{old('delivery')}}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label"><sup>*</sup>№ Отделение или адрес доставки</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" name="address" value="{{old('address')}}"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-2">Ваш комментарий по заказу</label>
                                                        <div class="col-sm-10">
                                                            <textarea rows="10" class="form-control" name="user_comments">{{old('user_comments')}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <!-- Start Shipping -->
                                </div>
                            </div>
                            <!-- Accordion end -->
                            <div class="shopping-checkout">
                                <button class="btn btn-primary pull-right" onclick="return confirm('Оформить заказ?')" form="deliveryData" data-toggle="tooltip">
                                    Оформить заказ
                                </button>
                                <!--<a href="#" class="btn btn-primary pull-right">Оформить заказ</a>-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Shopping-Cart -->
            </div>
        </div>
    </div>
    <!-- START BRAND-LOGO-AREA -->
    <div class="brand-logo-area carosel-navigation">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="area-title">
                        <h3 class="title-group border-red gfont-1">Brand Logo</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="active-brand-logo">
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/4.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/5.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/6.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END BRAND-LOGO-AREA -->
    <!-- START SUBSCRIBE-AREA -->
    <div class="subscribe-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <label class="hidden-sm hidden-xs">Sign Up for Our Newsletter:</label>
                    <div class="subscribe">
                        <form action="#">
                            <input type="text" placeholder="Enter Your E-mail">
                            <button type="submit">Subscribe</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="social-media">
                        <a href="#"><i class="fa fa-facebook fb"></i></a>
                        <a href="#"><i class="fa fa-google-plus gp"></i></a>
                        <a href="#"><i class="fa fa-twitter tt"></i></a>
                        <a href="#"><i class="fa fa-youtube yt"></i></a>
                        <a href="#"><i class="fa fa-linkedin li"></i></a>
                        <a href="#"><i class="fa fa-rss rs"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SUBSCRIBE-AREA -->
</section>
<!-- END PAGE-CONTENT -->
@endsection