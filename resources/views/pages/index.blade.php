@extends('layout')

@section('content')
<!-- Category slider area start -->
<div class="category-slider-area">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
            @include('pages._menu')
            </div>
            <div class="col-md-9">
            @include('pages._slider')
            </div>
        </div>
    </div>
</div>
<!-- Category slider area End -->
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3">
            @include('pages._sidebar')
            </div>
            <div class="col-md-9 col-sm-9">
                <!-- START PRODUCT-AREA (1) -->
                @foreach($categoriesOfProductsForMeanPage as $category)
                <div class="product-area">
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <!-- Start Product-Menu -->
                            <div class="product-menu">
                                <div class="product-title">
                                    <h3 class="title-group-2 gfont-1">{{$category->name}}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Product-Menu -->
                    <div class="clear"></div>
                    <!-- Start Product -->
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="product carosel-navigation">
                                <div class="tab-content">
                                    <!-- Product = display-1-1 -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="display-1-1">
                                        <div class="row">
                                            <div class="active-product-carosel">
                                                <!-- Start Single-Product -->
                                                @foreach($productsForMeanPage as $product)
                                                @if($category->id == $product->categories_id)
                                                <div class="col-xs-12">
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="{{route('getProductDetails', $product->slug)}}">
                                                                <img class="primary-img" src="{{asset('uploads/'.$product->images[0])}}" alt="Product">
                                                                <img class="secondary-img" src="{{asset('uploads/'.$product->images[0])}}" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="{{route('getProductDetails', $product->slug)}}">{{$product->name}}</a></h5>
                                                            <div class="price-box">
                                                                @if($product->discount != 0)
                                                                <span class="price">{{$product->discountPrice}}</span>
                                                                <span class="old-price">{{$product->price}}</span>
                                                                @else
                                                                <span class="price">{{$product->price}}</span>
                                                                @endif
                                                            </div>
<!--                                                            <span class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </span>-->
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button">
                                                                        <button type="submit" data-id="{{$product->id}}" class="toch-button toch-add-cart"><i class="fa fa-shopping-cart"></i>В корзину</button>
                                                                    </div>
<!--                                                                    <div class="product-button-2">
                                                                        <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                        <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>
                                                                        <a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
                                                                    </div>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Product -->
                </div>
                @endforeach
                <!-- END PRODUCT-AREA (1) -->
                <!-- START PRODUCT-BANNER -->
                <!-- END PRODUCT-BANNER -->
                <!-- START  -->
                <!-- START SMALL-PRODUCT-AREA (1) -->
                <div class="small-product-area">
                    <!-- Start Product-Menu -->
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="product-menu">
                                <ul role="tablist">
                                    <li role="presentation"><a href="#"  role="tab" data-toggle="tab">Возможно заинтересует</a></li>
<!--                                    <li role="presentation" class=" active"><a href="#display-4-1" role="tab" data-toggle="tab">Latest</a></li>
                                    <li role="presentation"><a href="#display-4-2" role="tab" data-toggle="tab">Sale</a></li>-->
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Product-Menu -->
                    <!-- Start Product -->
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="product carosel-navigation">
                                <div class="tab-content">
                                    <!-- Product = display-4-1 -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="display-4-1">
                                        <div class="row">
                                            <div class="active-small-product">
                                                <!-- Start Single-Product -->
                                                @foreach($rendomProducts as $products)
                                                 
                                                <div class="col-xs-12">
                                                   @foreach($products as $product)
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="{{route('getProductDetails', $product->slug)}}">
                                                                <img class="primary-img" src="{{asset('uploads/'.$product->images[0])}}" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="{{route('getProductDetails', $product->slug)}}">{{$product->name}}</a></h5>
                                                            <div class="price-box">
                                                                @if($product->discount != 0)
                                                                <span class="price">{{$product->discountPrice}}</span>
                                                                <span class="old-price">{{$product->price}}</span>
                                                                @else
                                                                <span class="price">{{$product->price}}</span>
                                                                @endif
                                                            </div>
<!--                                                            <span class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </span>-->
                                                            <div class="product-action">
                                                                <div class="product-button-2">
                                                                    <!--<a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>-->
                                                                    <button type="submit" data-id="{{$product->id}}" class="toch-button toch-add-cart"><i class="fa fa-shopping-cart"></i></button>
<!--                                                                    <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                    <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   @endforeach
                                                </div>
                                                
                                                @endforeach
                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-4-1 -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Product -->
                </div>
                <!-- END SMALL-PRODUCT-AREA (1) -->
            </div>
        </div>
    </div>
    <br/>
    <!-- START BRAND-LOGO-AREA -->
<!--    <div class="brand-logo-area carosel-navigation">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="area-title">
                        <h3 class="title-group border-red gfont-1">Brand Logo</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="active-brand-logo">
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/4.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/5.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/6.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <!-- END BRAND-LOGO-AREA -->
    <!-- START SUBSCRIBE-AREA -->
    <div class="subscribe-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <label class="hidden-sm hidden-xs">Sign Up for Our Newsletter:</label>
                    <div class="subscribe">
                        <form action="#">
                            <input type="text" placeholder="Enter Your E-mail">
                            <button type="submit">Subscribe</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="social-media">
                        <a href="#"><i class="fa fa-facebook fb"></i></a>
                        <a href="#"><i class="fa fa-google-plus gp"></i></a>
                        <a href="#"><i class="fa fa-twitter tt"></i></a>
                        <a href="#"><i class="fa fa-youtube yt"></i></a>
                        <a href="#"><i class="fa fa-linkedin li"></i></a>
                        <a href="#"><i class="fa fa-rss rs"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SUBSCRIBE-AREA -->
</section>
@endsection
