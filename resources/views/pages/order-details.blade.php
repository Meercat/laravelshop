@extends('layout')

@section('content')
<!-- START PAGE-CONTENT -->
<section class="page-content">
    <div class="container">
<!--        <div class="row">
            <div class="col-md-12">
                <ul class="page-menu">
                    <li><a href="index.html">Home</a></li>
                    <li class="active"><a href="#">Shopping Cart</a></li>
                </ul>
            </div>
        </div>-->
        <div class="row">
            <div class="col-md-3">
                <!-- CATEGORY-MENU-LIST START -->
                @include('pages._cabinet-menu')
                <!-- END CATEGORY-MENU-LIST -->
            </div>
            <div class="col-md-9">
                <!-- Start Shopping-Cart -->
                <div class="shopping-cart">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cart-title">
                                <h2 class="title-group-3 gfont-1">Заказ №{{$orderId}}</h2>
                                <hr>
                            </div>
                            <!-- Start Table -->
                            Получатель: {{$order->surname}} {{$order->name}} {{$order->second_name}}<br/>
                            Контактный телефон: {{$order->phone}}<br/>
                            Cлужба доставки: {{$order->delivery}}<br/>
                            Адрес доставки: {{$order->address}}<br/>
                            Статус заказа: {{$order->status}}<br/><br/>
                            Ваш комментарий: {{$order->user_comments}}<br/><br/>
                            Комментарий магазина: {!! $order->admin_comments  !!}<br/>
                            <hr>
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td class="text-center">Товар</td>
                                            <td class="text-left">Название</td>
                                            <td class="text-left">Количество</td>
                                            <td class="text-right">Цена за единицу товара</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($items as $item)
                                        <tr>
                                            <td class="text-center">
                                                <a href="{{route('getProductDetails', $item->product->slug)}}"><img class="img-thumbnail" src="{{asset('uploads/' . $item->images[0])}}" alt="{{$item->product->name}}" /></a>
                                            </td>
                                            <td class="text-center">
                                                <a href="{{route('getProductDetails', $item->product->slug)}}">{{$item->product->name}}</a>
                                            </td>
                                            <td>
                                                {{$item->quantity}}
                                            </td>
                                            <td>
                                                {{$item->price}}
                                            </td>
                                            @endforeach
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- End Table -->
                                                        <div class="row">
                                <div class="col-sm-4 col-sm-offset-8">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td class="text-right">
                                                    <strong>К оплате:</strong>
                                                </td>
                                                <td class="text-right">{{$totalPriceOfOrder}} грн.</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Shopping-Cart -->
            </div>
        </div>
    </div>
    <!-- START BRAND-LOGO-AREA -->
    <div class="brand-logo-area carosel-navigation">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="area-title">
                        <h3 class="title-group border-red gfont-1">Brand Logo</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="active-brand-logo">
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/4.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/5.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/6.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END BRAND-LOGO-AREA -->
    <!-- START SUBSCRIBE-AREA -->
    <div class="subscribe-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <label class="hidden-sm hidden-xs">Sign Up for Our Newsletter:</label>
                    <div class="subscribe">
                        <form action="#">
                            <input type="text" placeholder="Enter Your E-mail">
                            <button type="submit">Subscribe</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="social-media">
                        <a href="#"><i class="fa fa-facebook fb"></i></a>
                        <a href="#"><i class="fa fa-google-plus gp"></i></a>
                        <a href="#"><i class="fa fa-twitter tt"></i></a>
                        <a href="#"><i class="fa fa-youtube yt"></i></a>
                        <a href="#"><i class="fa fa-linkedin li"></i></a>
                        <a href="#"><i class="fa fa-rss rs"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SUBSCRIBE-AREA -->
</section>
<!-- END PAGE-CONTENT -->
@endsection