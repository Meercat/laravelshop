@extends('layout')

@section('content')
<!-- START PAGE-CONTENT -->
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="page-menu">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="#">cameras & photography</a></li>
                    <li class="active"><a href="#">Toch Prond</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <!-- CATEGORY-MENU-LIST START -->
                @include('pages._menu')
                <!-- END CATEGORY-MENU-LIST -->
                <!-- START SMALL-PRODUCT-AREA -->
                @include('pages._sidebar')
                <!-- END SMALL-PRODUCT-AREA -->
                <!-- START SIDEBAR-BANNER -->
                <div class="sidebar-banner hidden-sm hidden-xs">
                    <div class="active-sidebar-banner">
                        <div class="single-sidebar-banner">
                            <a href="#"><img src="img/banner/1.jpg" alt="Product Banner"></a>
                        </div>
                        <div class="single-sidebar-banner">
                            <a href="#"><img src="img/banner/2.jpg" alt="Product Banner"></a>
                        </div>
                    </div>
                </div>
                <!-- END SIDEBAR-BANNER -->
            </div>
            <div class="col-md-9 col-sm-12 col-xs-12">
                <!-- Start Toch-Prond-Area -->
                <div class="toch-prond-area">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="clear"></div>
                            <div class="tab-content">
                                <!-- Product = display-1-1 -->
                                <div role="tabpanel" class="tab-pane fade in active" id="display-0">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="toch-photo">
                                                <a href="#"><img src="{{asset('uploads/' . $product->images[0])}}" data-imagezoom="true" alt="{{$product->name . '-0'}}" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Product = display-1-1 -->
                                <!-- Start Product = display-1-2 -->
                                @for($i = 1; $i < count($product->images); $i++)
                                <div role="tabpanel" class="tab-pane fade" id="display-{{$i}}">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="toch-photo">
                                                <a href="#"><img src="{{asset('uploads/' . $product->images[$i])}}" data-imagezoom="true" alt="{{$product->name . '-' . $i}}" /></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endfor
                                <!-- End Product = display-1-2 -->
                            </div>
                            <!-- Start Toch-prond-Menu -->
                            <div class="toch-prond-menu">
                                <ul role="tablist">
                                    <li role="presentation" class=" active"><a href="#display-0" role="tab" data-toggle="tab"><img src="{{asset('uploads/' . $product->images[0])}}" alt="{{$product->name . '-0'}}" /></a></li>
                                    @for($i = 1; $i < count($product->images); $i++)
                                    <li role="presentation"><a href="#display-{{$i}}" role="tab" data-toggle="tab"><img src="{{asset('uploads/' . $product->images[$i])}}" alt="{{$product->name . '-' . $i}}" /></a></li>
                                    @endfor
                                </ul>
                            </div>
                            <!-- End Toch-prond-Menu -->
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <h2 class="title-product"> {{$product->name}}</h2>
                            <div class="about-toch-prond">
                                <p>
                                    <span class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                    <a href="#">1 reviews</a>
                                    /
                                    <a href="#">Write a review</a>
                                </p>
                                <hr />
                                <p class="toch-description">{!! $product->description !!}</p>
                                <hr />
                                @if($product->discount !=0)
                                <span class="price gfont-2">{{$product->discountPrice}}</span>
                                <span class="old-price gfont-2">{{$product->price}}</span>
                                @else
                                <span class="price gfont-2">{{$product->price}}</span>
                                @endif
                            </div>
                            <div class="product-quantity">
                                <span>Количество</span>
                                <input type="number" data-quantity="{{$product->id}}" value="1"  min="0" max="1000"/>
                                <button type="submit" data-id="{{$product->id}}" class="toch-button toch-add-cart"><i class="fa fa-shopping-cart"></i> В корзину</button>
                                <hr />
                            </div>
                        </div>
                    </div>
                    <!-- Start Toch-Box -->
                    <div class="toch-box">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- Start Toch-Menu -->
                                <div class="toch-menu">
                                    <ul role="tablist">
                                        <li role="presentation" class=" active"><a href="#description" role="tab" data-toggle="tab">Отзывы</a></li>
                                    </ul>
                                </div>
                                <!-- End Toch-Menu -->
                                <div class="tab-content toch-description-review">
                                    <!-- Start display-reviews -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="reviews">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="toch-reviews">
                                                    <div class="toch-table">
                                                        <table class="table table-striped table-bordered">
                                                            <tbody>
                                                                <tr>
                                                                    <td><strong>plaza theme</strong></td>
                                                                    <td class="text-right"><strong>16/02/2016</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <p>It is part of Australia's network of offshore processing centres for irregular migrants who arrive by boat, and also houses New Zealanders facing deportation from Australia.</p>
                                                                        <span><i class="fa fa-star"></i></span>
                                                                        <span><i class="fa fa-star"></i></span>
                                                                        <span><i class="fa fa-star"></i></span>
                                                                        <span><i class="fa fa-star"></i></span>
                                                                        <span><i class="fa fa-star-o"></i></span>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="toch-review-title">
                                                        <h2>Write a review</h2>
                                                    </div>
                                                    <div class="review-message">
                                                        <div class="col-xs-12">
                                                            <p><sup>*</sup>Your Name <br>
                                                                <input type="text" class="form-control" />
                                                            </p>
                                                            <p><sup>*</sup>Your Name <br>
                                                                <textarea class="form-control"></textarea>
                                                            </p>
                                                        </div>
                                                        <div class="help-block">
                                                            <span class="note">Note:</span>
                                                            HTML is not translated!
                                                        </div>
                                                        <div class="get-rating">
                                                            <span><sup>*</sup>Rating </span>
                                                            Bad
                                                            <input type="radio" value="1" name="rating" />
                                                            <input type="radio" value="2" name="rating" />
                                                            <input type="radio" value="3" name="rating" />
                                                            <input type="radio" value="4" name="rating" />
                                                            <input type="radio" value="5" name="rating" />
                                                            Good
                                                        </div>
                                                        <div class="buttons clearfix">
                                                            <button class="btn btn-primary pull-right">Continue</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-reviews -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Toch-Box -->
                <!-- START SMALL-PRODUCT-AREA (1) -->
                <div class="small-product-area">
                    <!-- Start Product-Menu -->
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="product-menu">
                                <ul role="tablist">
                                    <li role="presentation"><a href="#"  role="tab" data-toggle="tab">Возможно заинтересует</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- End Product-Menu -->
                    <!-- Start Product -->
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <div class="product carosel-navigation">
                                <div class="tab-content">
                                    <!-- Product = display-4-1 -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="display-4-1">
                                        <div class="row">
                                            <div class="active-small-product">
                                                <!-- Start Single-Product -->
                                                @foreach($similarProducts as $products)

                                                <div class="col-xs-12">
                                                   @foreach($products as $product)
                                                    <div class="single-product">
                                                        <div class="product-img">
                                                            <a href="{{route('getProductDetails', $product->slug)}}">
                                                                <img class="primary-img" src="{{asset('uploads/'.$product->images[0])}}" alt="Product">
                                                            </a>
                                                        </div>
                                                        <div class="product-description">
                                                            <h5><a href="{{route('getProductDetails', $product->slug)}}">{{$product->name}}</a></h5>
                                                            <div class="price-box">
                                                                @if($product->discountPrice != 0)
                                                                <span class="price">{{$product->discountPrice}}</span>
                                                                <span class="old-price">{{$product->price}}</span>
                                                                @else
                                                                <span class="price">{{$product->price}}</span>
                                                                @endif
                                                            </div>
<!--                                                            <span class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </span>-->
                                                            <div class="product-action">
                                                                <div class="product-button-2">
                                                                    <a href="#" data-toggle="tooltip" title="Add Cart"><i class="fa fa-shopping-cart"></i></a>
<!--                                                                    <a href="#" data-toggle="tooltip" title="Wishlist"><i class="fa fa-heart-o"></i></a>
                                                                    <a href="#" data-toggle="tooltip" title="Compare"><i class="fa fa-signal"></i></a>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   @endforeach
                                                </div>

                                                @endforeach
                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Product = display-4-1 -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Product -->
                </div>
                <!-- END SMALL-PRODUCT-AREA (1) -->
                </div>
                <!-- End Toch-Prond-Area -->
            </div>
        </div>
    </div>
    <!-- START BRAND-LOGO-AREA -->
    <div class="brand-logo-area carosel-navigation">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="area-title">
                        <h3 class="title-group border-red gfont-1">Brand Logo</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="active-brand-logo">
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/4.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/5.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/6.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END BRAND-LOGO-AREA -->
    <!-- START SUBSCRIBE-AREA -->
    <div class="subscribe-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <label class="hidden-sm hidden-xs">Sign Up for Our Newsletter:</label>
                    <div class="subscribe">
                        <form action="#">
                            <input type="text" placeholder="Enter Your E-mail">
                            <button type="submit">Subscribe</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="social-media">
                        <a href="#"><i class="fa fa-facebook fb"></i></a>
                        <a href="#"><i class="fa fa-google-plus gp"></i></a>
                        <a href="#"><i class="fa fa-twitter tt"></i></a>
                        <a href="#"><i class="fa fa-youtube yt"></i></a>
                        <a href="#"><i class="fa fa-linkedin li"></i></a>
                        <a href="#"><i class="fa fa-rss rs"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SUBSCRIBE-AREA -->
</section>
<!-- END PAGE-CONTENT -->
@endsection