@extends('layout')

@section('content')
<!-- START PAGE-CONTENT -->
<section class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <!-- CATEGORY-MENU-LIST START -->
                @include('pages._menu')
                <!-- END CATEGORY-MENU-LIST -->
                <!-- START SMALL-PRODUCT-AREA -->
                @include('pages._sidebar')
                <!-- END SMALL-PRODUCT-AREA -->
            </div>
            <div class="col-md-9">
                <!-- START PRODUCT-BANNER -->
<!--                <div class="product-banner">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="banner">
                                <a href="#"><img src="img/banner/12.jpg" alt="Product Banner"></a>
                            </div>
                        </div>
                    </div>
                </div>-->
                <!-- END PRODUCT-BANNER -->
                <!-- START PRODUCT-AREA -->
                <h3 class="title-group-3 gfont-1">{{$category->name}}</h3>
                <div class="product-area">
                    <div class="row">
                        <div class="col-xs-12">
                            <!-- Start Product-Menu -->
                            <div class="product-filter">
                                <ul role="tablist">
                                    <li role="presentation" class="list active">
                                        <a href="#display-1-1" role="tab" data-toggle="tab"></a>
                                    </li>
                                    <li role="presentation"  class="grid ">
                                        <a href="#display-1-2" role="tab" data-toggle="tab"></a>
                                    </li>
                                </ul>
                                <div class="sort">
                                    <label>Сортировать по:</label>
                                    <select>
                                        <option value="#">Default</option>
                                        <option value="#">Name (A - Z)</option>
                                        <option value="#">Name (Z - A)</option>
                                        <option value="#">Price (Low > High)</option>
                                        <option value="#">Rating (Highest)</option>
                                        <option value="#">Rating (Lowest)</option>
                                        <option value="#">Name (A - Z)</option>
                                        <option value="#">Model (Z - A))</option>
                                        <option value="#">Model (A - Z)</option>
                                    </select>
                                </div>
                                <div class="limit">
                                    <label>На странице:</label>
                                    <select>
                                        <option value="#" <?php if($productsOnPage == 7) echo 'selected'; ?>>7</option>
                                        <option value="#" <?php if($productsOnPage == 10) echo 'selected'; ?>>10</option>
                                        <option value="#" <?php if($productsOnPage == 25) echo 'selected'; ?>>25</option>
                                        <option value="#" <?php if($productsOnPage == 50) echo 'selected'; ?>>50</option>
                                        <option value="#" <?php if($productsOnPage == 100) echo 'selected'; ?>>100</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <!-- End Product-Menu -->
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                            <!-- Start Product -->
                            <div class="product">
                                <div class="tab-content">
                                    <!-- Product -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="display-1-1">
                                        <div class="row">
                                            <div class="listview">
                                                <!-- Start Single-Product -->
                                                @foreach($products as $product)
                                                <div class="single-product">
                                                    <div class="col-md-3 col-sm-4 col-xs-12">
<!--                                                        <div class="label_new">
                                                            <span class="new">new</span>
                                                        </div>-->
                                                        @if($product->discount != 0)
                                                        <div class="sale-off">
                                                            <span class="sale-percent">{{$product->discount}}%</span>
                                                        </div>
                                                        @endif
                                                        <div class="product-img">
                                                            <a href="{{route('getProductDetails', $product->slug)}}">
                                                                <img class="primary-img" src="{{asset('uploads/' . $product->images[0])}}" alt="{{$product->name}}">
                                                                <img class="secondary-img" src="{{asset('uploads/' . $product->images[0])}}" alt="{{$product->name}}">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9 col-sm-8 col-xs-12">
                                                        <div class="product-description">
                                                            <h5><a href="{{route('getProductDetails', $product->slug)}}">{{$product->name}}</a></h5>
                                                            <div class="price-box">
                                                                @if($product->discount !=0)
                                                                <span class="price gfont-2">{{$product->discountPrice}}</span>
                                                                <span class="old-price gfont-2">{{$product->price}}</span>
                                                                @else
                                                                <span class="price gfont-2">{{$product->price}}</span>
                                                                @endif
                                                            </div>
                                                            <span class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star-o"></i>
                                                            </span>
                                                            <p class="short-description">{{$product->description}}</p>
                                                            <div class="product-action">
                                                                <div class="button-group">
                                                                    <div class="product-button">
                                                                        <button type="submit" data-id="{{$product->id}}" class="toch-button toch-add-cart"><i class="fa fa-shopping-cart"></i> В корзину</button>
                                                                    </div>
<!--                                                                    <div class="product-button-2">
                                                                        <a href="#" class="modal-view" data-toggle="modal" data-target="#productModal" title="Quickview"><i class="fa fa-search-plus"></i></a>
                                                                    </div>-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                <!-- End Single-Product -->
                                            </div>
                                        </div>
                                        <!-- Start Pagination Area -->
                                        <div class="pagination-area">
                                            <div class="row">
                                                <div class="col-xs-5">
                                                    <div class="pagination">
                                                        <ul>
                                                        {{$products->render()}}
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-xs-7">
                                                    <div class="product-result">
                                                        <span>Showing 1 to 16 of {!! $products->total() !!} ({!! $products->lastPage()!!} Pages)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Pagination Area -->
                                    </div>
                                    <!-- End Product -->
                                    </div>
                                    <!-- End Product = TV -->
                                </div>
                            </div>
                            <!-- End Product -->
                        </div>
                    </div>
                </div>
                <!-- END PRODUCT-AREA -->
            </div>
        </div>
    </div>
    <!-- START BRAND-LOGO-AREA -->
    <div class="brand-logo-area carosel-navigation">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="area-title">
                        <h3 class="title-group border-red gfont-1">Brand Logo</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="active-brand-logo">
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/4.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/5.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/6.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="single-brand-logo">
                            <a href="#"><img src="img/brand/3.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END BRAND-LOGO-AREA -->
    <!-- START SUBSCRIBE-AREA -->
    <div class="subscribe-area">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7 col-xs-12">
                    <label class="hidden-sm hidden-xs">Sign Up for Our Newsletter:</label>
                    <div class="subscribe">
                        <form action="#">
                            <input type="text" placeholder="Enter Your E-mail">
                            <button type="submit">Subscribe</button>
                        </form>
                    </div>
                </div>
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="social-media">
                        <a href="#"><i class="fa fa-facebook fb"></i></a>
                        <a href="#"><i class="fa fa-google-plus gp"></i></a>
                        <a href="#"><i class="fa fa-twitter tt"></i></a>
                        <a href="#"><i class="fa fa-youtube yt"></i></a>
                        <a href="#"><i class="fa fa-linkedin li"></i></a>
                        <a href="#"><i class="fa fa-rss rs"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END SUBSCRIBE-AREA -->
</section>
<!-- END PAGE-CONTENT -->
@endsection