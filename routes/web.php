<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductsController@index')->name('products.index');

Route::any('/search', 'ProductsController@search')->name('products.search');

Route::get('/category/{productsOnPage}/{slug}', 'ProductsController@getProductsOfCategory')->name('getProductsOfCategory');

Route::get('/subcategory/{productsOnPage}/{slug}', 'ProductsController@getProductsOfSubcategory')->name('getProductsOfSubcategory');

Route::get('/prodact-details/{slug}', 'ProductsController@getProductDetails')->name('getProductDetails');

Route::any('/cart', 'CartsController@index')->name('carts.index');
Route::any('/cart/addItem', 'CartsController@addItemAjax')->name('carts.addItemAjax');
Route::any('/cart/{id}/remove-item-in-cart', 'CartsController@removeItemInCart')->name('carts.removeItemInCart');
Route::any('/cart/{id}/update-item-quantity-in-cart', 'CartsController@updateItemQuantityInCart')->name('carts.updateItemQuantityInCart');
Route::get('/clean-cart', 'CartsController@removeAllItems')->name('carts.removeAllItems');
Route::any('/new-orders', 'OrdersController@addNewOrder')->name('orders.addNewOrder');

Route::group(['middleware' => 'auth'], function(){
Route::get('/cabinet', 'CabinetsController@index')->name('cabinets.index');
Route::get('/cabinet/all-orders', 'CabinetsController@getAllOrders')->name('cabinets.getAllOrders');
Route::get('/cabinet/order/{id}', 'CabinetsController@getOrderDetails')->middleware('isLogin');
});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function(){
    Route::resource('/', 'DashboardController');
    Route::any('/products/update-price-from-elektreka', 'ProductsController@updatePriceFromElektreka')->name('products.updatePriceFromElektreka');
    Route::resource('/products', 'ProductsController');
    Route::resource('/categories', 'CategoriesController');
    Route::resource('/subcategories', 'SubcategoriesController');
    Route::resource('/orders', 'OrdersController');
    Route::any('/orders/{id}/add-new-item', 'OrdersController@addNewItemToOrder')->name('orders.addNewItemToOrder');
    Route::any('/orders/delete-item', 'OrdersController@destroyItem')->name('orders.destroyItem');
    Route::resource('/users', 'UsersController');
});

Auth::routes();

